import React, {useState} from 'react'
import { View, Text, ScrollView, TextInput, Button, StyleSheet, Image, Alert} from 'react-native'
import facebok from '../assets/icon_facebook.png'
import google from '../assets/icon_Google.png'
import usuario from '../assets/icon_user.png'


const RegistroForm = () => {

    const [state,setState]= useState({
        name:"",
        correo:"",
        contrasena:"",
    })

    const handleChangeSet = (name,value)=>{
        setState({...state, [name]:value});
    }
    return (
        <ScrollView style={styles.container}>
            <View style={{alignItems:'center'}}>
                <Image source={usuario} style={styles.iconoUser}/>
            </View>
            <View style={styles.icono}> 
            <View>
                <Image source={google}/>
            </View>
            <View>
                <Image source={facebok}/>
            </View>
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder='Nombre' onChangeText={(value)=> handleChangeSet('name',value)}/>
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder='Correo' keyboardType='email-address' onChangeText={(value)=> handleChangeSet('correo',value)}/>
            </View>
            <View style={styles.inputGroup}>
                <TextInput password = {true} secureTextEntry = {true} placeholder='Contraseña' onChangeText={(value)=> handleChangeSet('contrasena',value)}/>
            </View>
            <View style={styles.botonGroup}>
                <Button title='Registrar' onPress={()=> { Alert.alert('Usuario Resgistrado'); console.log(state)}} />
            </View>
            <View style={{alignItems:'center'}}>
                <Text style={{fontSize:22}}>Ó</Text>
            </View>
            <View style={styles.botonGroup}>
                <Button title='Iniciar Sesión'/>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container:{
        padding:35
    },
    inputGroup:{
        flex: 1,
        padding:0,
        margin:5,
        borderWidth:1,
        borderColor: '#ccc'
    },
    icono:{
        flexDirection:'row',
        justifyContent:'space-evenly',
        marginTop: 8,
        marginBottom: 7
    },
    iconoUser:{
        width: 160,
        height: 160,
    },
    botonGroup:{
        marginBottom:5,
        marginTop:5
    }
})

export default RegistroForm;